package com.simprints.simodkadapter


interface BasePresenter {

    fun start()

}
