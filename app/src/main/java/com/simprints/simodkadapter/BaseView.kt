package com.simprints.simodkadapter


interface BaseView<T> {

    var presenter: T

}