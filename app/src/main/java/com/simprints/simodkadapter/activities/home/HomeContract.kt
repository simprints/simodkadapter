package com.simprints.simodkadapter.activities.home

import com.simprints.simodkadapter.BasePresenter
import com.simprints.simodkadapter.BaseView


interface HomeContract {

    interface View : BaseView<Presenter>

    interface Presenter : BasePresenter

}

